package fr.shyndard.alteryaareamanagement.function;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.method.City;
import fr.shyndard.alteryaareamanagement.method.Plot;

public class Interaction {
	
	private static boolean canInteractBlock(Material mat) {
		if(mat == Material.FENCE_GATE 
				|| mat == Material.WOOD_PLATE 
				|| mat == Material.STONE_PLATE 
				|| mat == Material.STONE_PLATE 
				|| mat == Material.GOLD_PLATE 
				|| mat == Material.STONE_BUTTON 
				|| mat == Material.WOOD_BUTTON
				|| mat == Material.ENDER_CHEST) 
			return true;
		return false;
	}
	
	public static boolean canInteractBlock(Player player, Block block) {
		if(DataAPI.getPlayer(player).getRank().getPower() <= 3) return true;
		for(City city : Main.getCityList().values()) {
			if(city.getMayor() == player.getUniqueId()) {
				return true;
			}
			if(city.isInside(block.getLocation())) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(block.getLocation())) {
						if(plot.isMembre(player) || plot.isOwner(player)) return true;
						return block.getType() == Material.ENDER_CHEST;
					}
				}
				return canInteractBlock(block.getType());
			}
		}
		return SQLFunction.canInteractChunk(player, block.getLocation().getChunk().getX(), block.getLocation().getChunk().getZ());
	}

	public static boolean canBuild(Player player, Block block) {
		if(DataAPI.getPlayer(player).getRank().getPower() <= 3) return true;
		for(City city : Main.getCityList().values()) {
			if(city.getMayor() == player.getUniqueId()) {
				return true;
			}
			if(city.isInside(block.getLocation())) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(block.getLocation())) {
						if(plot.isMembre(player) || plot.isOwner(player)) return true;
						return false;
					}
				}
				return false;
			}
		}	
		return SQLFunction.canInteractChunk(player, block.getLocation().getChunk().getX(), block.getLocation().getChunk().getZ());
	}

	public static boolean canInteractPosition(Player player, Location loc) {
		if(DataAPI.getPlayer(player).getRank().getPower() <= 3) return true;
		for(City city : Main.getCityList().values()) {
			if(city.getMayor() == player.getUniqueId()) {
				return true;
			}
			if(city.isInside(loc)) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(loc)) {
						if(plot.isMembre(player) || plot.isOwner(player)) return true;
						return false;
					}
				}
				return false;
			}
		}	
		return SQLFunction.canInteractChunk(player, loc.getChunk().getX(), loc.getChunk().getZ());
	}
	
	public static boolean canMovePosition(Location to) {
		return false;
	}

	public static boolean canInteractPiston(Location loc) {
		for(City city : Main.getCityList().values()) {
			if(city.isInside(loc)) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(loc)) return true;
				}
				return false;
			}
		}	
		return true;
	}

	public static boolean isInsideCityButNotPlot(Location loc) {
		for(City city : Main.getCityList().values()) {
			if(city.isInside(loc)) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(loc)) return false;
				}
				return true;
			}
		}	
		return false;
	}

	public static boolean canPvp(Player target, Player damager) {
		if(DataAPI.getPlayer(damager).getRank().getPower() <= 1) return true;
		for(City city : Main.getCityList().values()) {
			if(city.isInside(target.getLocation()) && city.isInside(damager.getLocation())) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(target.getLocation()) && plot.isInside(damager.getLocation())) {
						return plot.getParam().isPvpEnabled();
					}
				}
				return false;
			}
		}	
		return false;
	}

	public static boolean isInsideCity(Location loc) {
		for(City city : Main.getCityList().values()) {
			if(city.isInside(loc)) return true;
		}	
		return false;
	}

	public static boolean canInteractEntity(Player player, Entity entity) {
		if(DataAPI.getPlayer(player).getRank().getPower() <= 3) return true;
		for(City city : Main.getCityList().values()) {
			if(city.isInside(entity.getLocation())) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(entity.getLocation())) return true;
				}
				return canInteractEntity(entity);
			}
		}	
		return SQLFunction.canInteractChunk(player, entity.getLocation().getChunk().getX(), entity.getLocation().getChunk().getZ());
	}

	private static boolean canInteractEntity(Entity entity) {
		switch(entity.getType()) {
			case MINECART:
				return true;
			default:
				return false;
		}
	}

	public static boolean canUseProjectile(Player player, Location loc) {
		if(DataAPI.getPlayer(player).getRank().getPower() <= 3) return true;
		for(City city : Main.getCityList().values()) {
			if(city.isInside(loc)) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(loc)) {
						if(plot.isMembre(player) || plot.isOwner(player) || plot.getParam().isPvpEnabled()) return true;
						return false;
					}
				}
				return false;
			}
		}
		return SQLFunction.canInteractChunk(player, loc.getChunk().getX(), loc.getChunk().getZ());
	}

	public static boolean isInsidePlot(Location loc) {
		return !SQLFunction.isChunkFree(loc.getChunk().getX(), loc.getChunk().getZ());
	}
}
