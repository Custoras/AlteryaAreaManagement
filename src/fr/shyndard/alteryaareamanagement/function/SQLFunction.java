package fr.shyndard.alteryaareamanagement.function;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.method.City;
import fr.shyndard.alteryaareamanagement.method.Plot;

public class SQLFunction {

	public static void loadCity() {
		Main.clearCity();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT `area_city_id`, `area_city_name`, `area_city_description`, `arena_city_loc_1_world`, `arena_city_loc_1_x`, `arena_city_loc_1_y`, `arena_city_loc_1_z`, `arena_city_loc_2_world`, `arena_city_loc_2_x`, `arena_city_loc_2_y`, `arena_city_loc_2_z`, (SELECT uuid FROM player_information WHERE id = area_city_mayor_player_id) AS mayor_uuid FROM area_city");
			while(result.next()) {
				try {
					Location loc1 = new Location(Bukkit.getWorld(result.getString("arena_city_loc_1_world")), result.getInt("arena_city_loc_1_x"), result.getInt("arena_city_loc_1_y"), result.getInt("arena_city_loc_1_z"));
					Location loc2 = new Location(Bukkit.getWorld(result.getString("arena_city_loc_2_world")), result.getInt("arena_city_loc_2_x"), result.getInt("arena_city_loc_2_y"), result.getInt("arena_city_loc_2_z"));
					Main.addCity(result.getInt("area_city_id"), new City(result.getInt("area_city_id"), result.getString("area_city_name"), loc1, loc2, result.getString("mayor_uuid")));
				} catch (SQLException e) {e.printStackTrace();}
			}
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void loadPlot() {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT (SELECT uuid FROM player_information WHERE id = area_plot_owner_player_id) as uuid, area_plot_cost, area_plot_id, area_city_id, area_plot_name, area_plot_loc_1_world, area_plot_loc_1_x, area_plot_loc_1_y, area_plot_loc_1_z, area_plot_loc_2_world, area_plot_loc_2_x, area_plot_loc_2_y, area_plot_loc_2_z, area_plot_category FROM area_plot");
			while(result.next()) {
				try {
					Location loc1 = new Location(Bukkit.getWorld(result.getString("area_plot_loc_1_world")), result.getInt("area_plot_loc_1_x"), result.getInt("area_plot_loc_1_y"), result.getInt("area_plot_loc_1_z"));
					Location loc2 = new Location(Bukkit.getWorld(result.getString("area_plot_loc_2_world")), result.getInt("area_plot_loc_2_x"), result.getInt("area_plot_loc_2_y"), result.getInt("area_plot_loc_2_z"));
					Main.getCityList().get(result.getInt("area_city_id")).addPlot(new Plot(result.getInt("area_plot_id"), result.getInt("area_city_id"), result.getString("area_plot_name"), loc1, loc2, (result.getString("uuid") == null ? null : UUID.fromString(result.getString("uuid"))), result.getInt("area_plot_category"), result.getInt("area_plot_cost")));
				} catch (SQLException e) {e.printStackTrace();}
			}
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static int addCity(String name, Location loc1, Location loc2) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO area_city (area_city_name, arena_city_loc_1_world, arena_city_loc_1_x, arena_city_loc_1_y, arena_city_loc_1_z, arena_city_loc_2_world, arena_city_loc_2_x, arena_city_loc_2_y, arena_city_loc_2_z) "
					+ "VALUES ('"+name+"', '"+loc1.getWorld().getName()+"', "+loc1.getBlockX()+", "+loc1.getBlockY()+", "+loc1.getBlockZ()+", '"+loc2.getWorld().getName()+"', "+loc2.getBlockX()+", "+loc2.getBlockY()+", "+loc2.getBlockZ()+")");
			state.close();
			
			state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT area_city_id FROM area_city ORDER BY area_city_id DESC LIMIT 1");
			while(result.next()) {
				return result.getInt("area_city_id");
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return 0;
	}
	
	public static int addPlot(String name, Integer city_id, Location loc1, Location loc2, Integer category, Integer money) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO area_plot (area_city_id , area_plot_name, area_plot_cost, area_plot_loc_1_world, area_plot_loc_1_x, area_plot_loc_1_y, area_plot_loc_1_z, area_plot_loc_2_world, area_plot_loc_2_x, area_plot_loc_2_y, area_plot_loc_2_z, area_plot_category) "
					+ "VALUES ("+city_id+", '"+name+"', '"+ money + "', '"+loc1.getWorld().getName()+"', "+loc1.getBlockX()+", "+loc1.getBlockY()+", "+loc1.getBlockZ()+", '"+loc2.getWorld().getName()+"', "+loc2.getBlockX()+", "+loc2.getBlockY()+", "+loc2.getBlockZ()+", "+category+")");
			state.close();
			
			state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT area_plot_id FROM area_plot ORDER BY area_plot_id DESC LIMIT 1");
			while(result.next()) {
				return result.getInt("area_plot_id");
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return 0;
	}
	
	public static void removeCity(City city) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM area_city WHERE area_city_id = " + city.getId());
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void removePlot(Plot plot) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM area_plot WHERE area_plot_id = " + plot.getId());
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void setOwner(Integer plot_id, UUID owner) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			if(owner == null)
				state.executeUpdate("UPDATE area_plot SET area_plot_owner_player_id = NULL WHERE area_plot_id = " + plot_id);
			else {
				state.executeUpdate("UPDATE area_plot SET area_plot_owner_player_id = (SELECT id FROM player_information WHERE uuid = '" + owner + "') WHERE area_plot_id = " + plot_id);
				state.executeUpdate("INSERT INTO area_payment VALUES ("+plot_id+", '"+(System.currentTimeMillis()/1000)+"', '1')");
			}
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void addPlotMembre(Integer plot_id, UUID membre) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO area_plot_membre VALUES ((SELECT id FROM player_information WHERE uuid = '" + membre + "'), " + plot_id + ")");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void removePlotMembre(Integer plot_id, UUID membre) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM area_plot_membre WHERE player_id = (SELECT id FROM player_information WHERE uuid = '" + membre + "') AND area_plot_id = " + plot_id);
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void removeAllMembre(Integer plot_id) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM area_plot_membre WHERE area_plot_id = " + plot_id);
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static Integer getPlayerPlotCount(Player player) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT COUNT(area_plot_id) AS plot_count FROM area_plot WHERE area_plot_owner_player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"')");
			while(result.next()) {
				return result.getInt("plot_count");
        	}
		} catch(Exception ex) { ex.printStackTrace(); }
		return 0;
	}

	public static boolean havePlotInCity(Player player, Integer city_id) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM area_plot WHERE area_city_id = " + city_id + " AND area_plot_owner_player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"')");
			while(result.next()) {
				return true;
        	}
		} catch(Exception ex) { ex.printStackTrace(); }
		return false;
	}

	public static void setMayor(Player target, Integer city_id) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE area_city SET area_city_mayor_player_id = (SELECT id FROM player_information WHERE uuid = '"+target.getUniqueId()+"') WHERE area_city_id = " + city_id);
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static void setPlotPrice(Integer plot_id, Integer price) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE area_plot SET area_plot_cost = "+price+" WHERE area_plot_id = " + plot_id);
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static boolean canInteractChunk(Player player, Integer x, Integer z) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM area_chunk AS AC WHERE `area_chunk_x` = "+x+" AND `area_chunk_z` = "+z+" " + 
					"AND " + 
					"(`area_chunk_owner_player_id` = " + 
					" (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') " + 
					" OR (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') " + 
					" IN (SELECT player_id FROM area_chunk_membre AS ACM WHERE ACM.area_chunk_id = AC.area_chunk_id)); ");
			while(result.next()) {
				return true;
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public static boolean isChunkFree(Integer x, Integer z) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM area_chunk WHERE area_chunk_x = "+x+" AND area_chunk_z = "+z);
			while(result.next()) return false;
		} catch (SQLException e) {e.printStackTrace(); return false;}
		return true;
	}
	
	public static void setChunkOwner(Player player, Integer x, Integer z, long time) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			if(player == null) state.executeUpdate("INSERT INTO area_chunk VALUES (NULL, NULL, " + x + ", " + z + ")");
			else {
				state.executeUpdate("INSERT INTO area_chunk VALUES (NULL, (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"'), " + x + ", " + z + ", " + ((System.currentTimeMillis()/1000)+time) + ")");
				state.executeUpdate("INSERT INTO area_payment VALUES ((SELECT area_chunk_id FROM area_chunk WHERE area_chunk_x = "+x+" AND area_chunk_z = "+z+"), '"+(System.currentTimeMillis()/1000)+"', '0')");
			}
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static boolean isChunkMember(Player player, Integer x, Integer z) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM area_chunk JOIN area_chunk_membre ON area_chunk.area_chunk_id = area_chunk_membre.area_chunk_id WHERE `area_chunk_x` = "+x+" AND `area_chunk_z` = "+z+" AND player_id = (SELECT id FROM player_information WHERE uuid = '" + player.getUniqueId() + "');");
			while(result.next()) {
				return true;
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public static boolean isChunkOwner(Player player, Integer x, Integer z) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM area_chunk WHERE area_chunk_owner_player_id = (SELECT id FROM player_information WHERE uuid = '" + player.getUniqueId() + "') AND area_chunk_x = " + x + " AND area_chunk_z = " + z);
			while(result.next()) {
				return true;
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public static void deleteChunk(Integer x, Integer z) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM area_chunk_membre WHERE area_chunk_id = (SELECT area_chunk_id FROM area_chunk WHERE area_chunk_x = " + x + " AND area_chunk_z = " + z + ")");
			state.executeUpdate("DELETE FROM area_chunk WHERE area_chunk_x = " + x + " AND area_chunk_z = " + z);
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static Integer getChunkCount(Player player) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT COUNT(area_chunk_id) AS count FROM area_chunk WHERE area_chunk_owner_player_id = (SELECT id FROM player_information WHERE uuid = '" + player.getUniqueId() + "')");
			while(result.next()) {
				return result.getInt("count");
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return 0;
	}
	
	public static List<Integer> getPlayerCityList(Player player) {
		List<Integer> list = new ArrayList<>();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT area_city_id FROM area_plot WHERE area_plot_owner_player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"');");
			while(result.next()) list.add(result.getInt("area_city_id"));
		} catch (SQLException e) {e.printStackTrace();}
		return list;
	}

	public static void setPlotName(Integer plot_id, String name) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE area_plot SET area_plot_name = "+name+" WHERE area_plot_id = " + plot_id);
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static void setPlotCategory(Integer plot_id, Integer category) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE area_plot SET area_plot_category = "+category+" WHERE area_plot_id = " + plot_id);
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static List<Plot> getPlayerPlotList(Player player) {
		List<Plot> list = new ArrayList<>();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT area_city_id, area_plot_id FROM area_plot WHERE area_plot_owner_player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"');");
			while(result.next()) {
				try {
					list.add(Main.getCityList().get(result.getInt("area_city_id")).getPlotList().get(result.getInt("area_plot_id")));
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch (SQLException e) {e.printStackTrace();}
		return list;
	}

	public static void addChunkMembre(Player target, int x, int z) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO area_chunk_membre VALUES ((SELECT area_chunk_id FROM area_chunk WHERE area_chunk_x = "+x+" AND area_chunk_z = "+z+"), (SELECT id FROM player_information WHERE uuid = '"+target.getUniqueId()+"'));");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void removeChunkMembre(Player target, int x, int z) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM area_chunk_membre WHERE area_chunk_id = (SELECT area_chunk_id FROM area_chunk WHERE area_chunk_x = "+x+" AND area_chunk_z = "+z+") AND player_id = (SELECT id FROM player_information WHERE uuid = '"+target.getUniqueId()+"');");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static boolean isSameOwner(Chunk c1, Chunk c2) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM area_chunk WHERE `area_chunk_x` = "+c1.getX()+" AND `area_chunk_z` = "+c1.getZ()+" AND area_chunk_owner_player_id IN (SELECT area_chunk_owner_player_id FROM area_chunk WHERE `area_chunk_x` = "+c2.getX()+" AND `area_chunk_z` = "+c2.getZ()+")");
			while(result.next()) return true;
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}

	public static void setPlotCategoryPrice(City city, int category, Integer price) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE area_plot SET area_plot_cost = " + price + " WHERE area_city_id = " + city.getId() + " AND area_plot_category = "+category +";");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
}
