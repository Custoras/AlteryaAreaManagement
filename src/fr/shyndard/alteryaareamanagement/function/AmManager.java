package fr.shyndard.alteryaareamanagement.function;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;

public class AmManager {

	public static void checkChunkPayment(Player player) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT `area_chunk_end_payment` AS end_payment, `area_chunk_id` AS id, `area_chunk_x` AS x, `area_chunk_z` AS z FROM area_chunk WHERE area_chunk_owner_player_id = " + DataAPI.getPlayer(player).getId() + " AND area_chunk_end_payment > 0 AND area_chunk_end_payment - 604800 < UNIX_TIMESTAMP();");
			while(result.next()) {
				player.sendMessage(ChatColor.RED + "Attention, votre claim (position " + result.getInt("x") + ", " + result.getInt("z") + ") va bient�t expirer.");
			}
			if(result.getRow() > 0) {
				player.sendMessage("Pour renouveller un claim, faites " + ChatColor.GREEN + "/claim renouveller");
			}
		} catch (SQLException e) {e.printStackTrace();}
	}
}