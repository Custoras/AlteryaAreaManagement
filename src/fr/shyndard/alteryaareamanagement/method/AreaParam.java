package fr.shyndard.alteryaareamanagement.method;

public class AreaParam {

	private boolean pvp = false;
	private boolean deathLoseMoney = true;
	private boolean interact = false;
	private boolean chestAccess = false;
	
	public AreaParam(int id) {
		loadParam(id);
	}
	
	private void loadParam(int id) {
		if(id == 0) {
			pvp = true;
			deathLoseMoney = false;
		} else {
			ParamManager.load(this, id);
		}
	}

	public boolean isPvpEnabled() { return pvp; }
	
	public boolean isDeathLoseMoneyEnabled() { return deathLoseMoney; }
	
	public boolean isInteractEnabled() { return interact; }
	
	public boolean isChestAccessEnabled() { return chestAccess; }

	public void setPvpEnabled(boolean value) { pvp = value; }
	
	public void setDeathLoseMoneyEnabled(boolean value) { deathLoseMoney = value; }
}
