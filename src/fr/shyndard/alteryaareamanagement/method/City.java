package fr.shyndard.alteryaareamanagement.method;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaareamanagement.function.SQLFunction;

public class City {

	int city_id = 0;
	String city_name;
	Area city_area;
	UUID mayor_uuid;
	Map<Integer, Plot> plot_list = new HashMap<>();
	
	public City(int id, String name, Location loc1, Location loc2, String uuid) {
		city_id = id;
		city_name = name;
		try { this.mayor_uuid = UUID.fromString(uuid); } catch (Exception ex) {}
		city_area = new Area(loc1, loc2);
	}
	
	public boolean isMayor(Player player) {
		return player.getUniqueId().equals(mayor_uuid);
	}
	public boolean isInside(Location loc) {
		return city_area.isInside(loc);
	}

	public int getId() {
		return city_id;
	}
	
	public String getName() {
		return city_name;
	}

	public void setName(String city_name) {
		this.city_name = city_name;
	}

	public Map<Integer, Plot> getPlotList() {
		return plot_list;
	}

	public void addPlot(Plot plot) {
		if(plot_list.get(plot.getId()) == null) plot_list.put(plot.getId(), plot);
	}
	
	public void removePlot(Plot plot) {
		SQLFunction.removePlot(plot);
		for(Iterator<Entry<Integer, Plot>> it = plot_list.entrySet().iterator(); it.hasNext(); ) {
			Entry<Integer, Plot> entry = it.next();
			if(entry.getKey() == plot.getId()) {
				it.remove();
			}
		}
	}

	public void setLocation(Location loc1, Location loc2) {
		city_area = new Area(loc1, loc2);
	}

	public void setMayor(Player mayor) {
		mayor_uuid = mayor.getUniqueId();
	}

	public UUID getMayor() {
		return mayor_uuid;
	}
}
