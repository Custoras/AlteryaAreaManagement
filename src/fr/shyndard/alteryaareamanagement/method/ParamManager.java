package fr.shyndard.alteryaareamanagement.method;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.shyndard.alteryaapi.api.DataAPI;

public class ParamManager {

	public static void load(AreaParam param, int id) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT param_id, param_value FROM area_param WHERE area_id = " + id + ";");
			while(result.next()) {
				try {
					switch(result.getInt("param_id")) {
						case 1:
							param.setPvpEnabled(result.getBoolean("param_value"));
							break;
						case 2:
							param.setDeathLoseMoneyEnabled(result.getBoolean("param_value"));
							break;
					}
				} catch (SQLException e) {e.printStackTrace();}
			}
		} catch (SQLException e) {e.printStackTrace();}
	}

}
