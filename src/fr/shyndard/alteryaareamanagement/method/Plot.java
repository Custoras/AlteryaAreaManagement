package fr.shyndard.alteryaareamanagement.method;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.function.SQLFunction;

public class Plot {

	Integer plot_id = 0;
	String plot_name;
	Area plot_area;
	UUID plot_owner;
	int plot_category;
	int plot_city_id;
	int plot_cost;
	List<UUID> plot_membre_list = new ArrayList<>();
	AreaParam param;
	
	public Plot(int id, int city_id, String name, Location loc1, Location loc2, UUID owner, Integer category, int cost) {
		plot_id = id;
		plot_name = name;
		plot_owner = owner;
		plot_category = category;
		plot_city_id = city_id;
		plot_cost = cost;
		plot_area = new Area(loc1, loc2);
		loadMembre();
		loadParam();
	}
	
	public boolean isOwner(Player player) {
		return player.getUniqueId().equals(plot_owner);
	}
	
	public boolean isMembre(Player player) {
		return plot_membre_list.contains(player.getUniqueId());
	}
	
	public boolean isInside(Location loc) {
		return plot_area.isInside(loc);
	}
	
	public Integer getId() {
		return plot_id;
	}
	
	public City getCity() {
		return Main.getCityList().get(plot_city_id);
	}

	public String getName() {
		return plot_name;
	}
	
	public int getCategory() {
		return plot_category;
	}

	public void setCategory(Integer plot_category) {
		this.plot_category = plot_category;
	}
	
	public void setName(String plot_name) {
		this.plot_name = plot_name;
	}

	public Integer getCost() {
		return plot_cost;
	}
	
	public void setCost(Integer plot_cost) {
		this.plot_cost = plot_cost;
	}
	
	public UUID getOwner() {
		return plot_owner;
	}

	public void setOwner(UUID plot_owner) {
		this.plot_owner = plot_owner;
		SQLFunction.setOwner(plot_id, plot_owner);
	}

	public List<UUID> getMembreList() {
		return plot_membre_list;
	}

	public void addMembre(UUID value, boolean sql) {
		if(!plot_membre_list.contains(value)) {
			plot_membre_list.add(value);
			if(sql) SQLFunction.addPlotMembre(plot_id, value);
		}
	}
	
	public void removeMembre(UUID value) {
		if(plot_membre_list.contains(value)) {
			plot_membre_list.remove(value);
			SQLFunction.removePlotMembre(plot_id, value);
		}
	}
	
	public void reset() {
		setOwner(new UUID(0,0));
		SQLFunction.removeAllMembre(plot_id);
		plot_membre_list.clear();
	}
	
	private void loadMembre() {
		plot_membre_list.clear();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT uuid FROM player_information, area_plot_membre WHERE id = player_id AND area_plot_id = " + plot_id);
			while(result.next()) {
				try {
					addMembre(UUID.fromString(result.getString("uuid")), false);
				} catch (SQLException e) {e.printStackTrace();}
			}
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public void setLocation(Location loc1, Location loc2) {
		plot_area = new Area(loc1, loc2);
	}
	
	public Area getArea() {
		return plot_area;
	}

	public AreaParam getParam() {
		return param;
	}

	public void loadParam() {
		param = new AreaParam(plot_id);
	}
}
