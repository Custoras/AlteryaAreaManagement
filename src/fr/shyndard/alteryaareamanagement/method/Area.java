package fr.shyndard.alteryaareamanagement.method;

import org.bukkit.Location;
import org.bukkit.World;

public class Area {
	int xmin;
	int xmax;
	int zmin;
	int zmax;
	World world;
	
	public Area(Location loc1, Location loc2) {
		world = loc1.getWorld();
		if(loc1.getBlockX() > loc2.getBlockX()) {
			xmin = loc2.getBlockX();
			xmax = loc1.getBlockX();
		} else {
			xmax = loc2.getBlockX();
			xmin = loc1.getBlockX();
		}
		if(loc1.getBlockZ() > loc2.getBlockZ()) {
			zmin = loc2.getBlockZ();
			zmax = loc1.getBlockZ();
		} else {
			zmax = loc2.getBlockZ();
			zmin = loc1.getBlockZ();
		}
	}
	
	public boolean isInside(Location loc) {
		return (world.equals(loc.getWorld()) && loc.getBlockX() >= xmin && loc.getBlockX() <= xmax && loc.getBlockZ() >= zmin && loc.getBlockZ() <= zmax);
	}
	
	public Location getPos1() {
		return new Location(world, xmin, 0, zmin);
	}
	
	public Location getPos2() {
		return new Location(world, xmax, 0, zmax);
	}
}
