package fr.shyndard.alteryaareamanagement.method;

import fr.shyndard.alteryaareamanagement.Main;

public class User {

	Integer position_city = 0;
	Integer position_plot = 0;

	public Integer getPositionCityId() {
		return position_city;
	}
	
	public Integer getPositionPlotId() {
		if(position_city == 0) return 0;
		return position_plot;
	}
	
	public Plot getPositionPlot() {
		if(position_plot == 0) return null;
		return getPositionCity().getPlotList().get(position_plot);
	}
	
	public City getPositionCity() {
		return Main.getCityList().get(position_city);
	}
	
	public void setPositionCityId(Integer position_city) {
		this.position_city = position_city;
	}
	
	public void setPositionPlotId(Integer position_plot) {
		this.position_plot = position_plot;
	}
}
