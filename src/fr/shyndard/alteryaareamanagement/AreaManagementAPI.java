package fr.shyndard.alteryaareamanagement;

import java.util.Map;

import org.bukkit.Location;

import fr.shyndard.alteryaareamanagement.method.AreaParam;
import fr.shyndard.alteryaareamanagement.method.City;
import fr.shyndard.alteryaareamanagement.method.Plot;

public class AreaManagementAPI {

	public static Map<Integer, City> getCityList() {
		return Main.getCityList();
	}
	
	public static AreaParam getActualLocationParam(Location loc) {
		for(City city : Main.getCityList().values()) {
			if(city.isInside(loc)) {
				for(Plot plot : city.getPlotList().values()) {
					if(plot.isInside(loc)) {
						return plot.getParam();
					}
				}
				return null;
			}
		}
		return null;
	}
}
