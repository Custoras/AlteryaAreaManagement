package fr.shyndard.alteryaareamanagement.event;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;

import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.function.Interaction;
import fr.shyndard.alteryaareamanagement.function.SQLFunction;

public class PistonInteract implements Listener {

	@EventHandler
	public void pistonInteract(BlockPistonExtendEvent event) {
		if(!event.getBlock().getWorld().equals(Bukkit.getWorlds().get(0))) return;
		Location loc = event.getBlock().getLocation();
		if(Interaction.isInsideCityButNotPlot(loc)) return;
		switch(event.getDirection()) {
			case EAST :
				loc.add(2,0,0);
				break;
			case WEST :
				loc.add(-2,0,0);
				break;
			case NORTH :
				loc.add(0,0,-2);
				break;
			case SOUTH :
				loc.add(0,0,2);
				break;
			default:
				break;
		}
		if(Interaction.canInteractPiston(loc)) {
			if(event.getBlock().getLocation().getChunk() != loc.getChunk() && !Interaction.isInsideCity(loc)) {
				if(!SQLFunction.isSameOwner(loc.getChunk(), event.getBlock().getLocation().getChunk())) {
					event.setCancelled(true);
				}
			}
		} else event.setCancelled(true);
		if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled() + " " + loc);
	}
}
