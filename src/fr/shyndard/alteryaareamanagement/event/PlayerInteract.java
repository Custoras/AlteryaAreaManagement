package fr.shyndard.alteryaareamanagement.event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Slime;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.function.Interaction;

public class PlayerInteract implements Listener {
	
	@EventHandler
	public void onInteract(BlockFromToEvent event) {
		if(event.getToBlock().getWorld() != Bukkit.getWorlds().get(0)) return;
		if(Main.option) event.setCancelled(!Interaction.canMovePosition(event.getToBlock().getLocation()));
		if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled());
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {	
		if(event.getPlayer().getWorld() != Bukkit.getWorlds().get(0)) return;
		if(!event.isCancelled()) {
			if(event.getClickedBlock() == null) {
				event.setCancelled(!Interaction.canInteractPosition(event.getPlayer(), event.getPlayer().getLocation()));
			} else {
				event.setCancelled(!Interaction.canInteractBlock(event.getPlayer(), event.getClickedBlock()));
			}
		}
		if(Main.debug) System.out.println(event.getEventName() + " " + event.getAction().name() + " " + event.getClickedBlock() + " " + event.isCancelled());
	}
	
	@EventHandler
	public void onDamageEntity(EntityDamageByEntityEvent event) {
		Entity sourceDamagerEntity = getEntitySourceOf(event.getDamager());
		if(sourceDamagerEntity.getType() == EntityType.PLAYER && event.getEntity().getType() == EntityType.PLAYER) {
			event.setCancelled(!Interaction.canPvp((Player)event.getEntity(), (Player)sourceDamagerEntity));
		}
		else if(event.getEntity().getWorld() == Bukkit.getWorlds().get(0)) {
			if(sourceDamagerEntity.getType() == EntityType.PLAYER) {
				if(!(event.getEntity() instanceof Monster)) {
					if(event.getEntity() instanceof Wolf) {
						Wolf w = (Wolf)event.getEntity();
						w.isTamed();
						return;
					} else if(event.getEntity() instanceof Slime) return;
					event.setCancelled(!Interaction.canInteractPosition((Player)sourceDamagerEntity, event.getEntity().getLocation()));
				}
			}
		}
		if(event.isCancelled() && event.getDamager() instanceof Arrow) {
			Arrow a = (Arrow)event.getDamager();
			if(a.getFireTicks() > 0) event.getEntity().setFireTicks(-20);
		}
		if(Main.debug) System.out.println(event.getEventName() + " " + event.getEntity() + " " + event.getDamager().getType().name() + " " + event.isCancelled());
	}

	@EventHandler
	public void onHanging(HangingBreakByEntityEvent event) {
		if(event.getEntity().getWorld() != Bukkit.getWorlds().get(0)) return;
		if(event.getRemover().getType() == EntityType.PLAYER) {
			event.setCancelled(!Interaction.canInteractPosition((Player)event.getRemover(), event.getEntity().getLocation()));
		} else {
			if(Interaction.isInsideCity(event.getEntity().getLocation()) || Interaction.isInsidePlot(event.getEntity().getLocation())) {
				event.setCancelled(true);
			}
		}
		if(Main.debug) System.out.println(event.getEventName() + " " + event.getEntity() + " " + event.getRemover() + " " + event.getCause().name() + " " + event.isCancelled());
	}

	@EventHandler
	public void onPotionLaunch(ProjectileLaunchEvent event) {
		if(event.getEntity().getWorld() != Bukkit.getWorlds().get(0)) return;
		 if (event.getEntity() instanceof ThrownPotion) {
			 ThrownPotion pot = (ThrownPotion) event.getEntity();
			 if(pot.getShooter() instanceof Player) {
				 Player player = (Player)pot.getShooter();
				 event.setCancelled(!Interaction.canUseProjectile(player, event.getEntity().getLocation()));
				 addPotion(player.getInventory(), pot.getItem());
			 }
			 if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled()); 
		 }
	}
	
	@EventHandler
	public void onBlockFrozed(EntityBlockFormEvent event) {
		if(event.getBlock().getWorld() != Bukkit.getWorlds().get(0)) return;
		if(event.getEntity().getType() == EntityType.PLAYER) {
			event.setCancelled(!Interaction.canInteractPosition((Player)event.getEntity(), event.getBlock().getLocation()));
		}
		if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled());
	}
	
	@EventHandler
	public void onInteractEntity(PlayerArmorStandManipulateEvent event) {
		if(event.getPlayer().getWorld() != Bukkit.getWorlds().get(0)) return;
		event.setCancelled(!Interaction.canInteractPosition(event.getPlayer(), event.getRightClicked().getLocation()));
		if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled());
	}
	
	@EventHandler
	public void onInteractEntity(PlayerInteractEntityEvent event) {
		if(event.getPlayer().getWorld() != Bukkit.getWorlds().get(0)) return;
		event.setCancelled(!Interaction.canInteractEntity(event.getPlayer(), event.getRightClicked()));
		if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled());
	}
	
	@EventHandler
	public void onPoseBlock(BlockPlaceEvent event) {
		if(event.getPlayer().getWorld() != Bukkit.getWorlds().get(0)) return;
		event.setCancelled(!Interaction.canBuild(event.getPlayer(), event.getBlock()));
		if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled());
	}
	
	@EventHandler
	public void onBreakBlock(BlockBreakEvent event) {
		if(event.getPlayer().getWorld() != Bukkit.getWorlds().get(0)) return;
		event.setCancelled(!Interaction.canBuild(event.getPlayer(), event.getBlock()));
		if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled());
	}
	
	@EventHandler
	public void onDestroyVehicule(VehicleDestroyEvent event) {
		if(event.getAttacker().getType() != EntityType.PLAYER) return;
		if(event.getAttacker().getWorld() != Bukkit.getWorlds().get(0)) return;
		event.setCancelled(!Interaction.canInteractPosition((Player)event.getAttacker(), event.getVehicle().getLocation()));
		if(Main.debug) System.out.println(event.getEventName() + " " + event.isCancelled());
	}
	
	private Entity getEntitySourceOf(Entity damager) {
		Entity source = damager;
		if(damager instanceof Projectile) {
			Projectile p = (Projectile)damager;
			if(p.getShooter() instanceof Entity) source = (Entity)p.getShooter();
		}
		return source;
	}
	
	private void addPotion(Inventory inv, ItemStack is) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
            public void run() {
                inv.addItem(is);
            }
        }, 2);
	}
}
