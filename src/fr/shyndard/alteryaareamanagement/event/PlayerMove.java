package fr.shyndard.alteryaareamanagement.event;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.shyndard.alteryaapi.method.Title;
import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.method.City;
import fr.shyndard.alteryaareamanagement.method.Plot;
import fr.shyndard.alteryaareamanagement.method.User;
import net.md_5.bungee.api.ChatColor;

public class PlayerMove implements Listener {
	
	@EventHandler
	public void playerMove(PlayerMoveEvent event) {
		if(event.getFrom().getBlockX() == event.getTo().getBlockX()
				&& event.getFrom().getBlockY() == event.getTo().getBlockY()
				&& event.getFrom().getBlockZ() == event.getTo().getBlockZ()) {
			return;
		}
		if(event.getPlayer().getWorld().equals(Bukkit.getWorlds().get(1))) return;
		User user = Main.user_list.get(event.getPlayer().getUniqueId());
		boolean inside = false;
		for(City city : Main.getCityList().values()) {
			if(city.isInside(event.getTo())) {
				if(user.getPositionCityId() != city.getId()) {
					user.setPositionCityId(city.getId());
					event.getPlayer().sendMessage(ChatColor.BLUE + "Vous entrez dans la ville " + ChatColor.GOLD + city.getName());
				}
				inside = true;
				break;
			}
		}
		
		if(!inside) {
			if(user.getPositionCityId() > 0) user.setPositionCityId(0);
			checkRemove18Pvp(event.getPlayer());
			return;
		}
		
		inside = false;
		
		for(Plot plot : user.getPositionCity().getPlotList().values()) {
			if(plot.isInside(event.getTo())) {
				if(user.getPositionPlotId() != plot.getId()) {
					user.setPositionPlotId(plot.getId());
					if(plot.getParam().isPvpEnabled()) {
						new Title(ChatColor.RED + "! Zone PvP !", "", 5, 30, 5).send(event.getPlayer());
						event.getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16);
					} else checkRemove18Pvp(event.getPlayer());
				}
				inside = true;
				break;
			}
		}
		
		if(!inside && user.getPositionPlotId() > 0) {
			user.setPositionPlotId(0);
			checkRemove18Pvp(event.getPlayer());
		}
	}
	
	private static void checkRemove18Pvp(Player player) {
		if(player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).getBaseValue() == 16) {
			player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(4);
		}
	}
}
