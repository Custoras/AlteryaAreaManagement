package fr.shyndard.alteryaareamanagement.event;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import fr.shyndard.alteryaareamanagement.function.Interaction;

public class MobSpawn implements Listener {

	@EventHandler
	public void onMobSpawn(CreatureSpawnEvent event) {
		if(event.getEntity() instanceof Monster || event.getEntityType() == EntityType.SLIME) {
			if(Interaction.isInsideCity(event.getEntity().getLocation()) 
					&& event.getSpawnReason() != SpawnReason.CUSTOM 
					&& event.getSpawnReason() != SpawnReason.SPAWNER
					&& event.getSpawnReason() != SpawnReason.SPAWNER_EGG) {
				event.setCancelled(true);
			}
		}
	}
}
