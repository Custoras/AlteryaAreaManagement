package fr.shyndard.alteryaareamanagement.event;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.method.User;

public class PlayerChangeWorld implements Listener {

	@EventHandler
	public void playerChangeWorld(PlayerChangedWorldEvent event) {
		if(!event.getFrom().equals(Bukkit.getWorlds().get(0))) return;
		User user = Main.user_list.get(event.getPlayer().getUniqueId());
		user.setPositionCityId(0);
		user.setPositionPlotId(0);
	}
}
