package fr.shyndard.alteryaareamanagement.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.function.AmManager;

public class PlayerConnection implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Main.addUser(event.getPlayer());
		AmManager.checkChunkPayment(event.getPlayer());
	}

}
