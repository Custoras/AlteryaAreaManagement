package fr.shyndard.alteryaareamanagement;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import fr.shyndard.alteryaareamanagement.command.CmdAm;
import fr.shyndard.alteryaareamanagement.command.CmdAmCheck;
import fr.shyndard.alteryaareamanagement.command.CmdChunk;
import fr.shyndard.alteryaareamanagement.command.CmdPlot;
import fr.shyndard.alteryaareamanagement.command.CmdPosition;
import fr.shyndard.alteryaareamanagement.event.MobSpawn;
import fr.shyndard.alteryaareamanagement.event.PistonInteract;
import fr.shyndard.alteryaareamanagement.event.PlayerChangeWorld;
import fr.shyndard.alteryaareamanagement.event.PlayerConnection;
import fr.shyndard.alteryaareamanagement.event.PlayerInteract;
import fr.shyndard.alteryaareamanagement.event.PlayerMove;
import fr.shyndard.alteryaareamanagement.function.SQLFunction;
import fr.shyndard.alteryaareamanagement.method.City;
import fr.shyndard.alteryaareamanagement.method.User;

public class Main extends JavaPlugin {

	static Map<Integer, City> city_list = new HashMap<>();
	public static Map<UUID, User> user_list = new HashMap<>();
	public static boolean debug;
	public static boolean option;
	
	static Main plugin;
	
	public void onEnable() {
		plugin = this;
		
		getCommand("am").setExecutor(new CmdAm());
		getCommand("chunk").setExecutor(new CmdChunk());
		getCommand("plot").setExecutor(new CmdPlot());
		getCommand("position").setExecutor(new CmdPosition());
		getCommand("amcheck").setExecutor(new CmdAmCheck());
		
		getServer().getPluginManager().registerEvents(new MobSpawn(), this);
		getServer().getPluginManager().registerEvents(new PlayerConnection(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteract(), this);
		getServer().getPluginManager().registerEvents(new PlayerChangeWorld(), this);
		getServer().getPluginManager().registerEvents(new PlayerMove(), this);
		getServer().getPluginManager().registerEvents(new PistonInteract(), this);
		
		SQLFunction.loadCity();
		SQLFunction.loadPlot();
		
		getServer().createWorld(new WorldCreator("ressources"));
		getServer().createWorld(new WorldCreator("nether").environment(Environment.NETHER));
		getServer().createWorld(new WorldCreator("end").environment(Environment.THE_END));
	}
	
	public static Map<Integer, City> getCityList() {
		return city_list;
	}
	
	public static void addCity(Integer id, City city) {
		city_list.put(id, city);
	}
	
	public static Map<UUID, User> getUserList() {
		return user_list;
	}
	
	public static void addUser(Player player) {
		user_list.put(player.getUniqueId(), new User());
	}

	public static void clearCity() {
		city_list.clear();
	}

	public static void removeCity(City city) {
		SQLFunction.removeCity(city);
		for(Iterator<Entry<Integer, City>> it = city_list.entrySet().iterator(); it.hasNext(); ) {
			Entry<Integer, City> entry = it.next();
			if(entry.getKey() == city.getId()) {
				it.remove();
			}
		}
	}

	public static Plugin getPlugin() {
		return plugin;
	}
}
