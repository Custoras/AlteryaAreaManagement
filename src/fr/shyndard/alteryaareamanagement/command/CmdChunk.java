package fr.shyndard.alteryaareamanagement.command;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.FunctionAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaareamanagement.function.SQLFunction;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class CmdChunk implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return false;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("chunk.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return false;
		}
		if(player.getWorld().equals(Bukkit.getWorlds().get(1))) {
			sender.sendMessage(ChatColor.RED + "Fonctionnalit� d�sactiv�e dans ce monde.");
			return false;
		}
		if(args.length == 0) {
			sendHelp(sender);
		} else {
			if(args[0].equalsIgnoreCase("ajouter") && pi.hasPermission("chunk.playermanagement")) {
				if(args.length != 2) {
					sender.sendMessage(ChatColor.GRAY + "/claim ajouter <pseudo>");
					return true;
				}
				int x = player.getLocation().getChunk().getX();
				int z = player.getLocation().getChunk().getZ();
				if(!SQLFunction.isChunkOwner(player, x, z)) {
					sender.sendMessage(ChatColor.RED + "Vous devez �tre le propri�taire.");
					return true;
				}
				Player target = null;
				try {
					target = Bukkit.getPlayer(args[1]);
				} catch(Exception ex) {
					sender.sendMessage(ChatColor.RED + "Joueur introuvable.");
					return true;
				}
				if(target == player) {
					sender.sendMessage(ChatColor.RED + "Vous ne pouvez pas vous ajouter.");
					return true;
				}
				if(SQLFunction.isChunkMember(target, x, z)) {
					sender.sendMessage(ChatColor.RED + "Ce joueur est d�j� membre de ce claim.");
					return true;
				}
				SQLFunction.addChunkMembre(target, x, z);
				sender.sendMessage(ChatColor.GRAY + target.getName() + " a �t� " + ChatColor.GREEN + "ajout�" + ChatColor.GRAY + " au claim.");
				return true;
			}
			else if(args[0].equalsIgnoreCase("retirer") && pi.hasPermission("chunk.playermanagement")) {
				if(args.length != 2) {
					sender.sendMessage(ChatColor.GRAY + "/claim retirer <pseudo>");
					return true;
				}
				int x = player.getLocation().getChunk().getX();
				int z = player.getLocation().getChunk().getZ();
				if(!SQLFunction.isChunkOwner(player, x, z)) {
					sender.sendMessage(ChatColor.RED + "Vous devez �tre le propri�taire.");
					return true;
				}
				Player target = null;
				try {
					target = Bukkit.getPlayer(args[1]);
				} catch(Exception ex) {
					sender.sendMessage(ChatColor.RED + "Joueur introuvable.");
					return true;
				}
				if(!SQLFunction.isChunkMember(target, x, z)) {
					sender.sendMessage(ChatColor.RED + "Ce joueur n'est pas membre de ce claim.");
					return true;
				}
				SQLFunction.removeChunkMembre(target, x, z);
				sender.sendMessage(ChatColor.GRAY + target.getName() + " a �t� " + ChatColor.RED + "retir�" + ChatColor.GRAY + " du claim.");
				return true;
			}
			else if(args[0].equalsIgnoreCase("map")) {
				if(!pi.hasPermission("chunk.map")) {
					sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
					return false;
				}
				for(int z = player.getLocation().getChunk().getZ()-4; z <= player.getLocation().getChunk().getZ()+4; z++) {
					String line = "";
					for(int x = player.getLocation().getChunk().getX()-15; x <= player.getLocation().getChunk().getX()+15; x++) {
						line += (SQLFunction.isChunkFree(x, z) ? ChatColor.GREEN + "" : ChatColor.RED + "") + (x==player.getLocation().getChunk().getX() && z==player.getLocation().getChunk().getZ() ? ChatColor.BOLD + "+" : "-");
					}
					sender.sendMessage(line);
				}
				return false;
			}
			if(args[0].equalsIgnoreCase("requisitionner") && pi.hasPermission("chunk.requisitionner")) {
				if(args.length == 2 && args[1].equalsIgnoreCase("zone")) {
					Map<String, Location> player_marker = pi.getSelection();
					if(player_marker == null || player_marker.get("p1") == null || player_marker.get("p2") == null) {
						sender.sendMessage(ChatColor.RED + "Veuillez d�finir les marqueurs.");
						return false;
					}
					sender.sendMessage(ChatColor.GRAY + "R�quisition en cours...");
					int x_min = player_marker.get("p1").getChunk().getX();
					int x_max = player_marker.get("p2").getChunk().getX();
					int z_min = player_marker.get("p1").getChunk().getZ();
					int z_max = player_marker.get("p2").getChunk().getZ();
					
					if(x_min > x_max) {
						x_min = player_marker.get("p2").getChunk().getX();
						x_max = player_marker.get("p1").getChunk().getX();
					}
					if(z_min > z_max) {
						z_min = player_marker.get("p2").getChunk().getZ();
						z_max = player_marker.get("p1").getChunk().getZ();
					}
					for(int x = x_min; x <= x_max; x++) {
						for(int z = z_min; z <= z_max; z++) {
							forceClaim(x, z);
						}
					}
					pi.getSelection().clear();
					sender.sendMessage(ChatColor.GREEN + "Zone de r�quisition confirm�e.");
				} else {
					forceClaim(player.getLocation());
					sender.sendMessage(ChatColor.GREEN + "R�quisition confirm�e.");
				}
				return false;
			}
			else if(args[0].equalsIgnoreCase("louer")) {
				if(SQLFunction.isChunkFree(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ())) {
					Integer max_chunk_count = 6;
					if(pi.getRank().getId() <= 13) max_chunk_count = 10;
					if(pi.getRank().getId() <= 12) max_chunk_count = 14;
					int actualChunkCount = SQLFunction.getChunkCount(player);
					if(actualChunkCount >= max_chunk_count) {
						sender.sendMessage(ChatColor.RED + "Vous ne pouvez avoir plus de " + max_chunk_count + " chunk.");
						return true;
					} else if(args.length >= 4 && args[1].equals("confirm")) {
						int time = 0;
						try {
							if(player.getLocation().getChunk().getX() != Integer.parseInt(args[2]) || player.getLocation().getChunk().getZ() != Integer.parseInt(args[3])) {
								sender.sendMessage(ChatColor.RED + "Vous avez chang� de claim. Veuillez recommencer.");
								return false;
							}
							if(actualChunkCount >= 7) {
								if(args.length == 5) {
									try {
										time = Integer.parseInt(args[4]);
										if(time < 0 || time > 2) {
											sender.sendMessage(ChatColor.RED + "Vous ne pouvez r�server que pour 1 ou 2 mois.");
											return true;
										} else {
											int price = time*50;
											if(pi.getMoney(false) >= price) pi.removeMoney((float)price, true, false);
											time = time*(60*60*24*30);
										}
									} catch(Exception ex) {
										sender.sendMessage(ChatColor.RED + "Une erreur est survenue, veuillez recommencer");
										return true;
									}
								} else {
									sender.sendMessage(ChatColor.RED + "Vous �tes oblig� de payer pour avoir + de 7 chunk.");
									return true;
								}
							}
						} catch(Exception ex) {
							sender.sendMessage(ChatColor.RED + "Impossible de confirmer la location. Veuillez recommencer.");
							return false;
						}
						SQLFunction.setChunkOwner(player, player.getLocation().getChunk().getX() , player.getLocation().getChunk().getZ(), time);
						sender.sendMessage(ChatColor.GREEN + "Location confirm�e ! Profitez en bien.");
					} else if(actualChunkCount >= 7) {
						BaseComponent[] hoverText1 = new ComponentBuilder("Louer ce claim 1 mois (" + ChatColor.GOLD + "50$" + ChatColor.AQUA + ")").color(net.md_5.bungee.api.ChatColor.AQUA).create();
						BaseComponent[] hoverText2 = new ComponentBuilder("Louer ce claim 2 mois (" + ChatColor.GOLD + "100$" + ChatColor.AQUA + ")").color(net.md_5.bungee.api.ChatColor.AQUA).create();
				        HoverEvent hoverEvent1 = new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText1);
				        HoverEvent hoverEvent2 = new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText2);
				        ClickEvent clickEvent1 =  new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chunk louer confirm " + player.getLocation().getChunk().getX() + " " + player.getLocation().getChunk().getZ() + " 1");
				        ClickEvent clickEvent2 =  new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chunk louer confirm " + player.getLocation().getChunk().getX() + " " + player.getLocation().getChunk().getZ() + " 2");
				        BaseComponent[] message1 = new ComponentBuilder(ChatColor.GREEN + " - Louer ce claim 1 mois pour " + ChatColor.GOLD + "50$").event(hoverEvent1).event(clickEvent1).create();
				        BaseComponent[] message2 = new ComponentBuilder(ChatColor.GREEN + " - Louer ce claim 2 mois pour " + ChatColor.GOLD + "100$").event(hoverEvent2).event(clickEvent2).create();
				        player.sendMessage(ChatColor.GRAY + "S�lectionnez la m�thode de location :");
				        player.spigot().sendMessage(message1);
				        player.spigot().sendMessage(message2);
					} else {
						BaseComponent[] hoverText = new ComponentBuilder("Acheter ce claim gratuitement").color(net.md_5.bungee.api.ChatColor.AQUA).create();
				        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText);
				        ClickEvent clickEvent =  new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chunk louer confirm " + player.getLocation().getChunk().getX() + " " + player.getLocation().getChunk().getZ());
				        BaseComponent[] message = new ComponentBuilder(ChatColor.GREEN + " - Confirmer la r�quisition de ce claim").event(hoverEvent).event(clickEvent).create();
				        player.spigot().sendMessage(message);
					}
				} else {
					sender.sendMessage(ChatColor.RED + "Ce claim est indisponible.");
				}
				return false;
			}
			if(args[0].equalsIgnoreCase("abandonner")) {
				if(SQLFunction.isChunkFree(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ())) {
					sender.sendMessage(ChatColor.GRAY + "Ce claim est disponible.");
					FunctionAPI.question(player, ChatColor.GREEN + " - Louer cet espace (chunk 16*16)", "/chunk louer");
				} else {
					if(SQLFunction.isChunkOwner(player, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ())) {
						if(args.length == 4 && args[1].equals("confirm")) {
							try {
								if(player.getLocation().getChunk().getX() != Integer.parseInt(args[2]) || player.getLocation().getChunk().getZ() != Integer.parseInt(args[3])) {
									sender.sendMessage(ChatColor.RED + "Vous avez chang� de claim. Veuillez recommencer.");
									return false;
								}
							} catch(Exception ex) {
								sender.sendMessage(ChatColor.RED + "Impossible de confirmer la location. Veuillez recommencer.");
								return false;
							}
							SQLFunction.deleteChunk(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ());
							sender.sendMessage(ChatColor.GREEN + "Claim abandonn�e !");
						} else {
							BaseComponent[] hoverText = new ComponentBuilder("Abandonner ce claim").color(net.md_5.bungee.api.ChatColor.AQUA).create();
					        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText);
					        ClickEvent clickEvent =  new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/chunk abandonner confirm " + player.getLocation().getChunk().getX() + " " + player.getLocation().getChunk().getZ());;
					        BaseComponent[] message = new ComponentBuilder(ChatColor.WHITE + "[" + ChatColor.RED + ChatColor.BOLD + "!" + ChatColor.WHITE + "] " + ChatColor.GRAY + "Cliquez pour confirmer l'abandon.").event(hoverEvent).event(clickEvent).create();
					        player.spigot().sendMessage(message);
							sender.sendMessage(ChatColor.WHITE + "[" + ChatColor.RED + ChatColor.BOLD + "!" + ChatColor.WHITE + "] " + ChatColor.GRAY + "Une fois abandonn�, vous et vos co�quipiers ne pourrez plus acc�der � ce claim.");
						}
					} else {
						sender.sendMessage(ChatColor.GRAY + "Vous n'�tes pas propri�taire de ce claim.");
					}
				}
				return false;
			}
			sendHelp(sender);
		}
		return false;
	}
	
	static void forceClaim(Location loc) {
		forceClaim(loc.getChunk().getX(), loc.getChunk().getZ());
	}
	
	static void forceClaim(Integer x, Integer z) {
		if(!SQLFunction.isChunkFree(x, z)) {
			SQLFunction.deleteChunk(x, z);
		}
		SQLFunction.setChunkOwner(null, x, z, 0);
	}
	
	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ChatColor.GREEN + "/claim abandonner" + ChatColor.GRAY + ": Abandonner votre claim.");
		sender.sendMessage(ChatColor.GREEN + "/claim ajouter <pseudo> " + ChatColor.GRAY + ": Ajouter un joueur � votre claim.");
		sender.sendMessage(ChatColor.GREEN + "/claim retirer <pseudo> " + ChatColor.GRAY + ": Retirer un joueur de votre claim.");
	}
}
