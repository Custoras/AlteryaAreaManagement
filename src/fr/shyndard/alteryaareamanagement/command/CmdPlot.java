package fr.shyndard.alteryaareamanagement.command;

import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.function.SQLFunction;
import fr.shyndard.alteryaareamanagement.method.City;
import fr.shyndard.alteryaareamanagement.method.Plot;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class CmdPlot implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!player.hasPermission("plot.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return true;
		}
		if(player.getWorld().equals(Bukkit.getWorlds().get(1))) {
			sender.sendMessage(ChatColor.RED + "Fonctionnalit� d�sactiv�e dans ce monde.");
			return true;
		}
		Plot plot = Main.getUserList().get(player.getUniqueId()).getPositionPlot();
		if(args.length == 0) sendHelp(player);
		else {
			if(args[0].equalsIgnoreCase("help")) {
				sendHelp(player);
			}
			else if(args[0].equalsIgnoreCase("list")) {
				sendPlayerPlotList(player);
			}
			else if(plot == null) {
				sender.sendMessage(ChatColor.GRAY + "Vous devez vous positionner dans une parcelle.");
				return true;
			}
			else if(args[0].equalsIgnoreCase("loadParam") && pi.hasPermission("plot.loadParam")) {
				for(City c : Main.getCityList().values()) {
					for(Plot p : c.getPlotList().values()) {
						p.loadParam();
					}
				}
				sender.sendMessage(ChatColor.GREEN + "Chargement des param�tres parcelles effectu�s.");
			}
			else if(args[0].equalsIgnoreCase("ajouter") && pi.hasPermission("plot.playermanagement")) {
				if(args.length != 2) {
					sender.sendMessage(ChatColor.GRAY + "/parcelle ajouter <pseudo>");
					return true;
				}
				if(plot.getOwner() == null || !plot.getOwner().equals(player.getUniqueId())) {
					sender.sendMessage(ChatColor.RED + "Vous devez �tre le propri�taire.");
					return true;
				}
				Player target = null;
				try {
					target = Bukkit.getPlayer(args[1]);
				} catch(Exception ex) {
					sender.sendMessage(ChatColor.RED + "Joueur introuvable.");
					return true;
				}
				if(plot.isMembre(target)) {
					sender.sendMessage(ChatColor.RED + "Ce joueur fait d�j� partie de cette parcelle.");
					return true;
				}
				plot.addMembre(target.getUniqueId(), true);
				sender.sendMessage(ChatColor.GRAY + target.getName() + " a �t� " + ChatColor.GREEN + "ajout�" + ChatColor.GRAY + " � la parcelle.");
			}
			else if(args[0].equalsIgnoreCase("retirer") && pi.hasPermission("plot.playermanagement")) {
				if(args.length != 2) {
					sender.sendMessage(ChatColor.GRAY + "/parcelle retirer <pseudo>");
					return true;
				}
				if(plot.getOwner() == null || !plot.getOwner().equals(player.getUniqueId())) {
					sender.sendMessage(ChatColor.RED + "Vous devez �tre le propri�taire.");
					return true;
				}
				Player target = null;
				try {
					target = Bukkit.getPlayer(args[1]);
				} catch(Exception ex) {
					sender.sendMessage(ChatColor.RED + "Joueur introuvable.");
					return true;
				}
				if(!plot.getMembreList().contains(target.getUniqueId())) {
					sender.sendMessage(ChatColor.RED + "Ce joueur ne fait pas partie de cette parcelle.");
					return true;
				}
				plot.removeMembre(target.getUniqueId());
				sender.sendMessage(ChatColor.GRAY + target.getName() + " a �t� " + ChatColor.RED + "retir�" + ChatColor.GRAY + " de la parcelle.");
			}
			else if(args[0].equalsIgnoreCase("acheter")) {
				if(args.length == 3 && args[1].equalsIgnoreCase("confirm")) {
					Integer plot_id;
					try {
						plot_id = Integer.parseInt(args[2]);
					} catch(Exception ex) {
						sender.sendMessage(ChatColor.RED + "Allez dans votre parcelle et recommencez.");
						return true;
					}
					if(!plot_id.equals(plot.getId())) {
						sender.sendMessage(ChatColor.RED + "Veuillez recommencez.");
						return true;
					}
					if(plot.getOwner() != null) {
						sender.sendMessage(ChatColor.RED + "Cette parcelle est d�j� vendue.");
						return true;
					}
					if(pi.getMoney(false) < plot.getCost()) {
						sender.sendMessage(ChatColor.RED + "Vous n'avez pas assez d'argent.");
						return true;
					}
					Integer max_plot_count = 2;
					if(pi.getRank().getPower() <= 8) max_plot_count++;
					if(pi.getRank().getPower() <= 7) max_plot_count++;
					
					if(pi.getRank().getPower() > 1) {
						if(SQLFunction.getPlayerPlotCount(player) >= max_plot_count) {
							sender.sendMessage(ChatColor.RED + "Vous ne pouvez avoir plus de " + max_plot_count + " parcelles.");
							return true;
						}
						if(SQLFunction.havePlotInCity(player, plot.getCity().getId())) {
							sender.sendMessage(ChatColor.RED + "Vous ne pouvez avoir plus d'une parcelle par ville.");
							return true;
						}
						Integer player_category_access = 3;
						if(pi.getRank().getPower() <= 8) player_category_access--;
						if(pi.getRank().getPower() <= 7) player_category_access--;
						if(plot.getCategory() < player_category_access) {
							sender.sendMessage(ChatColor.RED + "Vous n'avez pas acc�s � ce type de parcelle.");
							return true;
						}
					}
					pi.removeMoney((float)plot.getCost(), true, false);
					plot.setOwner(player.getUniqueId());
					sender.sendMessage(ChatColor.GREEN + "Parcelle achet�e ! Profitez en bien.");
					return true;
				}
				if(plot.getOwner() != null) {
					sender.sendMessage(ChatColor.RED + "Cette parcelle est d�j� vendue.");
					return true;
				}
				if(pi.getMoney(false) < plot.getCost()) {
					sender.sendMessage(ChatColor.RED + "Vous n'avez pas assez d'argent.");
					return true;
				}				
				BaseComponent[] hoverText = new ComponentBuilder("Acheter la parcelle " + plot.getName() + " pour " + plot.getCost() + "$").color(net.md_5.bungee.api.ChatColor.AQUA).create();
		        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText);
		        ClickEvent clickEvent =  new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/plot acheter confirm " + plot.getId());
		        BaseComponent[] message = new ComponentBuilder(ChatColor.WHITE + "[" + ChatColor.RED + ChatColor.BOLD + "!" + ChatColor.WHITE + "] " + ChatColor.GRAY + "Cliquez pour valider l'achat.").event(hoverEvent).event(clickEvent).create();
		        player.spigot().sendMessage(message);
			}
			else if(args[0].equalsIgnoreCase("vendre")) {
				if(plot.getOwner() == null || !plot.getOwner().equals(player.getUniqueId())) {
					sender.sendMessage(ChatColor.RED + "Vous devez �tre le propri�taire.");
					return true;
				}
				if(args.length == 3 && args[1].equalsIgnoreCase("confirm")) {
					Integer plot_id;
					try {
						plot_id = Integer.parseInt(args[2]);
					} catch(Exception ex) {
						sender.sendMessage(ChatColor.RED + "Allez dans votre parcelle et recommencez.");
						return true;
					}
					if(plot_id.intValue() != plot.getId().intValue()) {
						sender.sendMessage(ChatColor.RED + "Veuillez recommencez.");
						return true;
					}
					for(UUID uuid : plot.getMembreList()) {
						try {
							Bukkit.getPlayer(uuid).sendMessage(ChatColor.RED + "Vous n'avez plus acc�s � la parcelle " + plot.getName() + ".");
						} catch(Exception ex) {}
					}
					plot.reset();
					pi.addMoney((float)((int)(plot.getCost()/3)*2), true, true);
					sender.sendMessage(ChatColor.GREEN + "Parcelle revendue !");
					return true;
				}
				BaseComponent[] hoverText = new ComponentBuilder("Vendre la parcelle " + plot.getName() + ".").color(net.md_5.bungee.api.ChatColor.AQUA).create();
		        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText);
		        ClickEvent clickEvent =  new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/plot vendre confirm " + plot.getId());
		        BaseComponent[] message = new ComponentBuilder(ChatColor.WHITE + "[" + ChatColor.RED + ChatColor.BOLD + "!" + ChatColor.WHITE + "] " + ChatColor.GRAY + "Cliquez pour confirmer la vente.").event(hoverEvent).event(clickEvent).create();
		        player.spigot().sendMessage(message);
		        sender.sendMessage(ChatColor.WHITE + "[" + ChatColor.RED + ChatColor.BOLD + "!" + ChatColor.WHITE + "] " + ChatColor.GRAY + "Une fois vendue, vous et vos co�quipiers ne pourrez plus acc�der � cette parcelle.");
			}
		}
		return true;
	}
	
	private void sendPlayerPlotList(Player player) {
		List<Plot> list = SQLFunction.getPlayerPlotList(player);
		player.sendMessage(DataAPI.getLine(ChatColor.GREEN));
		if(list.size() == 0) {
			player.sendMessage(ChatColor.GRAY + "Vous ne poss�dez aucune parcelle.");
		} else {
			player.sendMessage(ChatColor.GRAY + "Vous poss�dez " + list.size() + " parcelle" + (list.size() > 1 ? "s" : "") + ".");
			for(Plot plot : list) {
				player.sendMessage(ChatColor.GRAY + " - " + ChatColor.GOLD + plot.getCity().getName() + ChatColor.GRAY + " aux coordonn�es "+plot.getArea().getPos1().getBlockX()+", "+plot.getArea().getPos1().getBlockY()+", "+plot.getArea().getPos1().getBlockZ() + "");
			}
		}
		player.sendMessage(DataAPI.getLine(ChatColor.GREEN));
	}

	private void sendHelp(Player player) {
		player.sendMessage(ChatColor.GREEN + "/parcelle list " + ChatColor.GRAY + ": Informations sur vos parcelles.");
		player.sendMessage(ChatColor.GREEN + "/parcelle vendre" + ChatColor.GRAY + ": Vendre votre parcelle.");
		player.sendMessage(ChatColor.GREEN + "/parcelle ajouter <pseudo> " + ChatColor.GRAY + ": Ajouter un joueur � votre parcelle.");
		player.sendMessage(ChatColor.GREEN + "/parcelle retirer <pseudo> " + ChatColor.GRAY + ": Retirer un joueur de votre parcelle.");
	}

}
