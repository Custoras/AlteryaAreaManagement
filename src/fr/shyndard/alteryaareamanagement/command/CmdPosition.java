package fr.shyndard.alteryaareamanagement.command;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.FunctionAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.function.SQLFunction;
import fr.shyndard.alteryaareamanagement.method.User;

public class CmdPosition implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return false;
		}
		Player player = (Player)sender;
		if(!player.hasPermission("position.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return false;
		}
		if(player.getWorld().equals(Bukkit.getWorlds().get(1))) {
			sender.sendMessage(ChatColor.RED + "Fonctionnalit� d�sactiv�e dans ce monde.");
			return false;
		}
		User user = Main.getUserList().get(player.getUniqueId());
		sender.sendMessage(DataAPI.getLine(ChatColor.GREEN));
		if(user.getPositionCityId() == 0) {
			if(SQLFunction.isChunkFree(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ())) {
				sender.sendMessage(ChatColor.GREEN  + "Vous �tes dans la nature");
				FunctionAPI.question(player, ChatColor.GREEN + " - Louer ce claim (chunk 16*16)", "/chunk louer");
			} else if(SQLFunction.isChunkOwner(player, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ())) {
				sender.sendMessage(ChatColor.GRAY + "Vous �tes propri�taire de ce claim.");
				sender.sendMessage(ChatColor.GRAY + "Besoin d'aide ? Utilisez " + ChatColor.GREEN + "/claim");
			} else {
				sender.sendMessage(ChatColor.GRAY + "Ce claim est indisponible.");
			}
		} else {
			sender.sendMessage(ChatColor.GRAY + "Ville : " + ChatColor.GOLD + user.getPositionCity().getName());
			if(user.getPositionCity().getMayor() != null) sender.sendMessage(ChatColor.GRAY + "Maire : " + ChatColor.RED + DataAPI.getPlayerName(user.getPositionCity().getMayor()));
			if(user.getPositionPlot() != null) {
				PlayerInformation pi = DataAPI.getPlayer(player);
				if(pi.getRank().getPower() <= 2) {
					sender.sendMessage(ChatColor.GRAY + "Id : " + user.getPositionPlot().getId() + ", nom : " + user.getPositionPlot().getName());
				}
				if(new UUID(0,0).equals(user.getPositionPlot().getOwner())) {
					sender.sendMessage(ChatColor.RED + "Parcelle indisponible.");
					if(pi.getRank().getPower() <= 2) {
						FunctionAPI.question(player, ChatColor.GREEN + "> Remettre cette parcelle en vente <", "/am mettreenvente " + user.getPositionPlot().getId());
					}
				}
				else if(user.getPositionPlot().getOwner() == null) {
					sender.sendMessage(ChatColor.GRAY + "Prix : " + ChatColor.GOLD + user.getPositionPlot().getCost() + "$ " + ChatColor.GRAY + "(Cat�gorie " + user.getPositionPlot().getCategory() + ")");
					FunctionAPI.question(player, ChatColor.GOLD + "Acheter cette parcelle ?", "/plot acheter");
				} else {
					sender.sendMessage(ChatColor.GRAY + "Propri�taire : " + ChatColor.AQUA + DataAPI.getPlayerName(user.getPositionPlot().getOwner()));
					if(user.getPositionPlot().getOwner() == player.getUniqueId()) {
						sender.sendMessage(ChatColor.GRAY + "Besoin d'aide ? Utilisez " + ChatColor.GREEN + "/parcelle");
					}
				}
			}
		}
		sender.sendMessage(DataAPI.getLine(ChatColor.GREEN));
		return false;
	}
}
