package fr.shyndard.alteryaareamanagement.command;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaareamanagement.Main;
import fr.shyndard.alteryaareamanagement.function.SQLFunction;
import fr.shyndard.alteryaareamanagement.method.Area;
import fr.shyndard.alteryaareamanagement.method.City;
import fr.shyndard.alteryaareamanagement.method.Plot;
import fr.shyndard.alteryaareamanagement.method.User;
import net.md_5.bungee.api.ChatColor;

public class CmdAm implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return false;
		}
		Player player = (Player) sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if (!pi.hasPermission("am.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return false;
		}
		if (player.getWorld().equals(Bukkit.getWorlds().get(1))) {
			sender.sendMessage(ChatColor.RED + "Fonctionnalit� d�sactiv�e dans ce monde.");
			return false;
		} else if (args.length == 0) {
			sender.sendMessage(ChatColor.DARK_GRAY + "Commandes AreaManagement");
			sender.sendMessage(ChatColor.GRAY + "/am citylist");
			sender.sendMessage(ChatColor.GRAY + "/am mettreenvente");
			sender.sendMessage(ChatColor.GRAY + "/am plotlist <id_ville>");
			sender.sendMessage(ChatColor.GRAY + "/am createcity <name>");
			sender.sendMessage(ChatColor.GRAY + "/am createplot <id_ville> <nom> <categorie> <prix>");
			sender.sendMessage(ChatColor.GRAY + "/am setplotcost <id_ville> <id_parcelle> <prix>");
			sender.sendMessage(ChatColor.GRAY + "/am setplotcategorycost <id_ville> <categorie> <prix>");
			sender.sendMessage(ChatColor.GRAY + "/am setplotname <id_ville> <id_parcelle> <nom>");
			sender.sendMessage(ChatColor.GRAY + "/am setplotcategory <id_ville> <id_parcelle> <categorie>");
			sender.sendMessage(ChatColor.GRAY + "/am setmayor <id_ville> [pseudo]");
			sender.sendMessage(ChatColor.GRAY + "/am setcity <id_ville>");
			sender.sendMessage(ChatColor.GRAY + "/am setplot <id_ville> <id_parcelle>");
			sender.sendMessage(ChatColor.GRAY + "/am removeCity <id_ville>");
			sender.sendMessage(ChatColor.GRAY + "/am removePlot <id_ville> <id_parcelle>");
		} else {
			if (args[0].equalsIgnoreCase("pvpvalue")) {
				if(args[1].equals("get")) {
					player.sendMessage(player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).getBaseValue() + " value");
				} else if(args[1].equals("set")) {
					player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(Integer.parseInt(args[2]));
					player.sendMessage("set");
				}
			}
			else if (args[0].equalsIgnoreCase("reload")) {
				SQLFunction.loadCity();
				SQLFunction.loadPlot();
				sender.sendMessage(ChatColor.GREEN + "Chargement effectu�.");
			} else if (args[0].equalsIgnoreCase("debug")) {
				if (Main.debug)
					Main.debug = false;
				else
					Main.debug = true;
				sender.sendMessage(ChatColor.GREEN + "Debug " + Main.debug);
			} else if (args[0].equalsIgnoreCase("option")) {
				if (Main.option)
					Main.option = false;
				else
					Main.option = true;
				sender.sendMessage(ChatColor.GREEN + "Option " + Main.option);
			} else if (args[0].equalsIgnoreCase("mettreenvente")) {
				User user = Main.getUserList().get(player.getUniqueId());
				if (user.getPositionPlot() == null) {
					sender.sendMessage(ChatColor.RED + "Veuillez vous positionner dans une parcelle.");
				} else if (args.length != 2) {
					sender.sendMessage(ChatColor.RED + "Utilisation /am mettreenvente <id_parcelle>");
				} else if (user.getPositionPlot().getOwner() == null || !user.getPositionPlot().getOwner().equals(new UUID(0, 0))) {
					sender.sendMessage(ChatColor.RED + "Cette parcelle est d�j� en vente ou apparatient � un joueur.");
				} else {
					Integer plot_id = null;
					try {
						plot_id = Integer.parseInt(args[1]);
					} catch(Exception ex) {
						sender.sendMessage(ChatColor.RED + "Veuillez vous positionner dans une parcelle.");
						return true;
					}
					if(user.getPositionPlot().getId().intValue() != plot_id.intValue()) {
						sender.sendMessage(ChatColor.RED + "Mauvais identifiant de parcelle. Recommencez.");
					} else {
						user.getPositionPlot().setOwner(null);
						sender.sendMessage(ChatColor.GREEN + "Parcelle " + ChatColor.GOLD + user.getPositionPlot().getName() + ChatColor.GREEN + " remise en vente.");
					}
				}
			} else if (args[0].equalsIgnoreCase("citylist")) {
				sender.sendMessage(ChatColor.DARK_GRAY + "ID - Nom");
				for (City city : Main.getCityList().values()) {
					sender.sendMessage("" + ChatColor.GRAY + city.getId() + " - " + city.getName());
				}
			} else if (args[0].equalsIgnoreCase("plotlist")) {
				if (args.length != 2) {
					sender.sendMessage(ChatColor.GRAY + "/am plotlist <id_ville>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				if (city.getPlotList().size() == 0) {
					sender.sendMessage(ChatColor.RED + "Aucune parcelle d�finie pour cette ville.");
				} else {
					sender.sendMessage(ChatColor.GRAY + "Parcelle(s) de " + ChatColor.GOLD + city.getName());
					sender.sendMessage(ChatColor.DARK_GRAY + "ID - Nom - Categorie");
					for (Plot plot : city.getPlotList().values()) {
						sender.sendMessage("" + ChatColor.GRAY + plot.getId() + " - " + plot.getName() + " - "
								+ plot.getCategory() + " - " + plot.getCost() + "$");
					}
				}
			} else if (args[0].equalsIgnoreCase("reload")) {
				SQLFunction.loadCity();
				SQLFunction.loadPlot();
				sender.sendMessage(ChatColor.GREEN + "Chargement effectu�.");
			} else if (args[0].equalsIgnoreCase("removePlot")) {
				if (args.length != 3) {
					sender.sendMessage(ChatColor.GRAY + "/am removePlot <id_ville> <id_parcelle>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				Integer plot_id;
				try {
					plot_id = Integer.parseInt(args[2]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier positif.");
					return false;
				}
				Plot plot = city.getPlotList().get(plot_id);
				if (plot == null) {
					sender.sendMessage(ChatColor.RED + "Parcelle inconnue. V�rifiez via /am plotlist <id_ville>");
					return false;
				}
				city.removePlot(plot);
				sender.sendMessage(
						ChatColor.GREEN + "Parcelle " + plot.getName() + " (" + plot.getId() + ") supprim�e.");
			} else if (args[0].equalsIgnoreCase("removeCity")) {
				if(pi.getRank().getPower() > 1) {
					sender.sendMessage(ChatColor.RED + "Vous n'�st pas autoris� � supprimer une ville. Contactez un Chancelier");
					return false;
				}
				if (args.length != 2) {
					sender.sendMessage(ChatColor.GRAY + "/am removePlot <id_ville>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				if (city.getPlotList().size() > 0) {
					sender.sendMessage(ChatColor.RED + "Il ne doit plus y avoir de parcelle dans cette ville.");
					return false;
				}
				Main.removeCity(city);
				sender.sendMessage(ChatColor.GREEN + "Ville " + city.getName() + " (" + city.getId() + ") supprim�e.");
			} else if (args[0].equalsIgnoreCase("setcity")) {
				if (args.length != 2) {
					sender.sendMessage(ChatColor.GRAY + "/am setcity <id_ville>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier ville positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				if(pi.getRank().getPower() > 1 && !player.getUniqueId().equals(city.getMayor())) {
					sender.sendMessage(ChatColor.RED + "Vous n'�st pas autoris� � �diter cette ville.");
					return false;
				}
				Map<String, Location> player_marker = DataAPI.getPlayer(player).getSelection();
				if (player_marker == null || player_marker.get("p1") == null || player_marker.get("p2") == null) {
					sender.sendMessage(ChatColor.RED + "Veuillez d�finir les marqueurs.");
					return false;
				}
				Area temp_area = new Area(player_marker.get("p1"), player_marker.get("p2"));
				for (Plot plot : city.getPlotList().values()) {
					if (!temp_area.isInside(plot.getArea().getPos1())
							|| !temp_area.isInside(plot.getArea().getPos2())) {
						sender.sendMessage(ChatColor.RED + "Une parcelle est en dehors de la nouvelle s�lection.");
						return false;
					}
				}
				city.setLocation(player_marker.get("p1"), player_marker.get("p2"));
				DataAPI.getPlayer(player).getSelection().clear();
				sender.sendMessage(ChatColor.GREEN + "Dimensions de la ville " + city.getName() + " �dit�es !");
			} else if (args[0].equalsIgnoreCase("setplotcost")) {
				if (args.length != 4) {
					sender.sendMessage(ChatColor.GRAY + "/am setPlotCost <id_ville> <id_parcelle> <prix>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier ville positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				Integer plot_id;
				try {
					plot_id = Integer.parseInt(args[2]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier parcelle positif.");
					return false;
				}
				Plot plot = city.getPlotList().get(plot_id);
				if (plot == null) {
					sender.sendMessage(ChatColor.RED + "Parcelle inconnue. V�rifiez via /am plotlist <id_ville>");
					return false;
				}
				Integer price = null;
				try {
					price = Integer.parseInt(args[3]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier prix positif.");
					return false;
				}
				if (price < 0) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier prix positif.");
					return false;
				}
				SQLFunction.setPlotPrice(plot.getId(), price);
				plot.setCost(price);
				sender.sendMessage(ChatColor.GRAY + "La parcelle " + ChatColor.YELLOW + plot.getName() + ChatColor.GRAY
						+ " vaut d�sormais " + ChatColor.GOLD + price + "$");
			} else if (args[0].equalsIgnoreCase("setplotcategorycost")) {
				if (args.length != 4) {
					sender.sendMessage(ChatColor.GRAY + "/am setPlotCost <id_ville> <categorie> <prix>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier ville positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				int category = 0;
				try {
					category = Integer.parseInt(args[2]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier parcelle positif.");
					return false;
				}
				if (category < 1 || category > 3) {
					sender.sendMessage(ChatColor.RED + "Veuillez saisir une valeur entre 1 et 3");
					return false;
				}
				Integer price = null;
				try {
					price = Integer.parseInt(args[3]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier prix positif.");
					return false;
				}
				if (price <= 0) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier prix sup�rieur � 0.");
					return false;
				}
				SQLFunction.setPlotCategoryPrice(city, category, price);
				for(Plot plot : city.getPlotList().values()) if(plot.getCategory() == category) plot.setCost(price);
				sender.sendMessage(ChatColor.GRAY + "Les parcelles de cat�gorie " + category + " � " + city.getName() 
					+ " vallent d�sormais " + ChatColor.GOLD + price + "$");
			} else if (args[0].equalsIgnoreCase("setplotname")) {
				if (args.length != 4) {
					sender.sendMessage(ChatColor.GRAY + "/am setplotname <id_ville> <id_parcelle> <nom>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier ville positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				Integer plot_id;
				try {
					plot_id = Integer.parseInt(args[2]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier parcelle positif.");
					return false;
				}
				Plot plot = city.getPlotList().get(plot_id);
				if (plot == null) {
					sender.sendMessage(ChatColor.RED + "Parcelle inconnue. V�rifiez via /am plotlist <id_ville>");
					return false;
				}
				SQLFunction.setPlotName(plot.getId(), args[3]);
				plot.setName(args[3]);
				sender.sendMessage(ChatColor.GRAY + "La parcelle s'appelle d�sormais " + ChatColor.YELLOW
						+ plot.getName() + ChatColor.GRAY + ".");
			} else if (args[0].equalsIgnoreCase("setplotcategory")) {
				if (args.length != 4) {
					sender.sendMessage(ChatColor.GRAY + "/am setplotcategory <id_ville> <id_parcelle> <categorie>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier ville positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				Integer plot_id;
				try {
					plot_id = Integer.parseInt(args[2]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier parcelle positif.");
					return false;
				}
				Plot plot = city.getPlotList().get(plot_id);
				if (plot == null) {
					sender.sendMessage(ChatColor.RED + "Parcelle inconnue. V�rifiez via /am plotlist <id_ville>");
					return false;
				}
				Integer category = null;
				try {
					category = Integer.parseInt(args[3]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier positif.");
					return false;
				}
				if (category < 1 || category > 3) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier prix positif.");
					return false;
				}
				SQLFunction.setPlotCategory(plot.getId(), category);
				plot.setCategory(category);
				sender.sendMessage(ChatColor.GRAY + "La parcelle " + ChatColor.YELLOW + plot.getName() + ChatColor.GRAY
						+ " est d�sormais de cat�gorie " + ChatColor.GOLD + category);
			} else if (args[0].equalsIgnoreCase("setplot")) {
				if (args.length != 3) {
					sender.sendMessage(ChatColor.GRAY + "/am setplot <id_ville> <id_parcelle>");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier ville positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				Integer plot_id;
				try {
					plot_id = Integer.parseInt(args[2]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier parcelle positif.");
					return false;
				}
				Plot plot = city.getPlotList().get(plot_id);
				if (plot == null) {
					sender.sendMessage(ChatColor.RED + "Parcelle inconnue. V�rifiez via /am plotlist <id_ville>");
					return false;
				}
				Map<String, Location> player_marker = DataAPI.getPlayer(player).getSelection();
				if (player_marker == null || player_marker.get("p1") == null || player_marker.get("p2") == null) {
					sender.sendMessage(ChatColor.RED + "Veuillez d�finir les marqueurs.");
					return false;
				}
				if (!city.isInside(player_marker.get("p1")) || !city.isInside(player_marker.get("p2"))) {
					sender.sendMessage(ChatColor.RED + "Parcelle en dehors de la ville " + city.getName() + ".");
					return false;
				}
				plot.setLocation(player_marker.get("p1"), player_marker.get("p2"));
				DataAPI.getPlayer(player).getSelection().clear();
				sender.sendMessage(ChatColor.GREEN + "Dimensions de la parcelle " + plot.getName() + " �dit�es !");
			} else if (args[0].equalsIgnoreCase("createcity")) {
				if (args.length != 2) {
					sender.sendMessage(ChatColor.GRAY + "/am createcity <nom>");
					return false;
				}
				boolean exist = false;
				for (City city : Main.getCityList().values()) {
					if (city.getName().equalsIgnoreCase(args[1])) {
						exist = true;
						break;
					}
				}
				if (exist) {
					sender.sendMessage(ChatColor.RED + "Ce nom de ville est d�j� utilis�.");
					return false;
				}
				Map<String, Location> player_marker = DataAPI.getPlayer(player).getSelection();
				if (player_marker == null || player_marker.get("p1") == null || player_marker.get("p2") == null) {
					sender.sendMessage(ChatColor.RED + "Veuillez d�finir les marqueurs.");
					return false;
				}
				int city_id = SQLFunction.addCity(args[1], player_marker.get("p1"), player_marker.get("p2"));
				Main.addCity(city_id, new City(city_id, args[1], player_marker.get("p1"), player_marker.get("p2"),
						player.getUniqueId().toString()));
				DataAPI.getPlayer(player).getSelection().clear();
				sender.sendMessage(ChatColor.GREEN + "Ville " + args[1] + " cr��e !");
			} else if (args[0].equalsIgnoreCase("createplot")) {
				if (args.length != 5) {
					sender.sendMessage(ChatColor.GRAY + "/am createplot <id_ville> <nom> <categorie> <prix>");
					return false;
				}
				Integer money;
				try {
					money = Integer.parseInt(args[4]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier prix positif.");
					return false;
				}
				if (money < 0) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier prix positif.");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier ville positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				Integer category;
				try {
					category = Integer.parseInt(args[3]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier categorie positif.");
					return false;
				}
				if (category > 3 || category < 1) {
					sender.sendMessage(ChatColor.RED + "Cat�gorie (taille) comprise entre 1 et 3.");
					return false;
				}
				boolean exist = false;
				for (Plot plot : city.getPlotList().values()) {
					if (plot.getName().equalsIgnoreCase(args[2])) {
						exist = true;
						break;
					}
				}
				if (exist) {
					sender.sendMessage(ChatColor.RED + "Ce nom de parcelle est d�j� utilis�.");
					return false;
				}
				Map<String, Location> player_marker = DataAPI.getPlayer(player).getSelection();
				if (player_marker == null || player_marker.get("p1") == null || player_marker.get("p2") == null) {
					sender.sendMessage(ChatColor.RED + "Veuillez d�finir les marqueurs.");
					return false;
				}
				if (!city.isInside(player_marker.get("p1")) || !city.isInside(player_marker.get("p2"))) {
					sender.sendMessage(ChatColor.RED + "Parcelle en dehors de la ville " + city.getName() + ".");
					return false;
				}
				int plot_id = SQLFunction.addPlot(args[2], city_id, player_marker.get("p1"), player_marker.get("p2"),
						category, money);
				city.addPlot(new Plot(plot_id, city.getId(), args[2], player_marker.get("p1"), player_marker.get("p2"),
						null, category, money));
				DataAPI.getPlayer(player).getSelection().clear();
				sender.sendMessage(ChatColor.GREEN + "Parcelle " + args[2] + " de cat�gorie " + category + " cr��e !");
			} else if (args[0].equalsIgnoreCase("setmayor")) {
				if (args.length <= 1 || args.length > 3) {
					sender.sendMessage(ChatColor.GRAY + "/am setmayor <id_ville> [pseudo]");
					return false;
				}
				Integer city_id;
				try {
					city_id = Integer.parseInt(args[1]);
				} catch (Exception ex) {
					sender.sendMessage(ChatColor.RED + "Veuillez indiquer un entier ville positif.");
					return false;
				}
				City city = Main.getCityList().get(city_id);
				if (city == null) {
					sender.sendMessage(ChatColor.RED + "Ville inconnue. V�rifiez via /am citylist");
					return false;
				}
				Player target = player;
				if (args.length == 3) {
					target = Bukkit.getPlayer(args[2]);
				}
				if (target == null) {
					sender.sendMessage(ChatColor.RED + "Joueur introuvable.");
					return false;
				}
				city.setMayor(target);
				SQLFunction.setMayor(target, city.getId());
				sender.sendMessage(ChatColor.GREEN + target.getName() + ChatColor.GRAY + " est d�sormais maire de "
						+ ChatColor.GOLD + city.getName());
			} else {
				sender.sendMessage(ChatColor.GRAY + "Argument inconnu. /am");
			}
		}
		return false;
	}

}
