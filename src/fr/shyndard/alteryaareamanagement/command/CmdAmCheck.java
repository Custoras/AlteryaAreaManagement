package fr.shyndard.alteryaareamanagement.command;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaareamanagement.AreaManagementAPI;

public class CmdAmCheck implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(sender instanceof Player) if(DataAPI.getPlayer((Player)sender).getRank().getPower() > 1) return false;
		int update = 0;
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT MAX(area_payment_date) as buy_date, area_plot_id FROM area_plot AS AP JOIN area_payment AS PA ON AP.area_plot_id = PA.area_id AND AP.area_city_id = 2 AND area_payment_type = 1 GROUP BY area_plot_id HAVING (buy_date+1296000) < UNIX_TIMESTAMP();");
			while(result.next()) {
				AreaManagementAPI.getCityList().get(2).getPlotList().get(result.getInt("area_plot_id")).setOwner(new UUID(0,0));
				update++;
			}
		} catch (SQLException e) {e.printStackTrace();}
		sender.sendMessage("Total of " + update + " tameryl update.");
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeQuery("DELETE FROM area_chunk WHERE area_chunk_id IN (SELECT area_chunk_id FROM area_chunk AS AC JOIN player_information AS PI ON area_chunk_owner_player_id = id WHERE area_chunk_end_payment > 0 AND area_chunk_end_payment < UNIX_TIMESTAMP());");
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
}